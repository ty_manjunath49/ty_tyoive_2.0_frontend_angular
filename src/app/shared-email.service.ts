import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedEmail } from './shared-email';
import { backendUrl } from '../app/config'

@Injectable({
  providedIn: 'root'
})
export class SharedEmailService {

  sharedEmail: SharedEmail;

  // api: ``;
  constructor(private http: HttpClient) { }

  getDetails(): Observable<any> {
    return this.http.get<any>(`${backendUrl}/invoice`);
  }

  insertDetails(sharedEmail): Observable<any> {
    return this.http.post<any>(`${backendUrl}/invoice`, sharedEmail);
  }

  updateDetails(sharedEmail): Observable<any> {
    return this.http.put<any>(`${backendUrl}/invoice`, sharedEmail);
  }

  deleteDetails(sharedEmail: SharedEmail): Observable<any> {
    return this.http.delete<any>(`${backendUrl}/invoice`);
  }

}
