import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  backendUrl = 'http://10.10.12.244:8080';
  constructor(private http: HttpClient) { }
  getEmployee(): Observable<any>{
    return this.http.get<any>(`${this.backendUrl}/employee/emps`);
  }
}
