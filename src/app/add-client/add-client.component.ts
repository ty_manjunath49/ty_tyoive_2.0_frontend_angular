import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { AddClientService } from '../add-client.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {

  form: FormGroup;
  formdata;
  message: string;
  datass: any;
 
  constructor(private addClient: AddClientService,
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient) { }

  ngOnInit() {

    this.form=this.fb.group({
      gstin : this.fb.control('',Validators.required),
      cfullname : this.fb.control('',Validators.required),
      cshortname : this.fb.control('',),
      panNo: this.fb.control('',Validators.required),
      cin : this.fb.control('',Validators.required),
      notes : this.fb.control('',Validators.required),
      clientDetails: this.fb.array([
        this.createSPOC()
      ])
    }   )
   }
  
   createSPOC(): FormGroup {
    return this.fb.group({
      clientName:'',
      phoneNumber:'',
      email:''
    });
  }

  // createSPOC() {
  //   let control=<FormArray>this.form.controls.clientDetails;
  //   this.datas.clientDetails.forEach(x => {
  //     control.push(this.fb.group({
  //       clientName:x.clientName,
  //       phoneNumber:this.createPhone(x),
  //       email:this.createemail(x),
  //     }))
  //   });
  //   }

  
  addSPOC(){
    let control = <FormArray>this.form.controls.clientDetails;
    control.push(this.fb.group({
      clientName:[''],
      phoneNumber:'',
      email:''
    }))
  }

  // addPhone(control) {
  //   control.push(this.fb.group({
  //     phoneNumber:['']
  //   }))
  // }

  // addemail(control) {
  //   control.push(this.fb.group({
  //     email:['']
  //   }))
    // this.clientDetails=this.form.get('clientDetails.email') as FormArray;
    // this.clientDetails.push(this.createemail());
  // }

  // // to delete each skill
  // deletespoc(i: number) {
  //   this.clientDetails.removeAt(i);
  // }

  getSPOCData(){
    return this.form.get('clientDetails') as FormArray;
  }
  
  get cfullname(): FormControl {
    return this.form.get('cfullname') as FormControl;
  }

  get cshortname(): FormControl {
    return this.form.get('cshortname') as FormControl;
  }

  get gstin(): FormControl {
    return this.form.get('gstin') as FormControl;
  }

  get cin(): FormControl {
    return this.form.get('cin') as FormControl;
  }

  get notes(): FormControl {
    return this.form.get('notes') as FormControl;
  }

  verifyGST(gst){
    console.log(gst.value);
    return this.http.get<any>('../../assets/another.json').subscribe(data=>{
      console.log(data);
      this.formdata=data;
      console.log(this.formdata.taxpayerInfo.lgnm)
      
      this.datass=data;
      this.form=this.fb.group({
        gstin : this.fb.control(gst.value,Validators.required),
        cfullname : this.fb.control(this.formdata.taxpayerInfo.lgnm,Validators.required),
        cshortname : this.fb.control('',),
        panNo: this.fb.control(this.formdata.taxpayerInfo.panNo,Validators.required),
        cin : this.fb.control('',Validators.required),
        notes : this.fb.control('',Validators.required),
        clientDetails : this.fb.array([this.createSPOC()])
      })
         }, err => {
      console.log(err)
    }, () => {
      console.log('GSTIN posted scccessfully')
    });
  }

  postClientDetails() {
    console.log(this.form.value);
    this.addClient.insertCompany(this.form.value).subscribe(data => {
      console.log(data);
      this.message = data.message;
      this.router.navigateByUrl('company-view');
      setTimeout(() => {
        this.message = null;
      }, 2000);
       this.form.reset();
    }, err => {
      console.log(err);
    });
  }

  addProject(){
    this.router.navigateByUrl('/add-spoc')
  }
}
