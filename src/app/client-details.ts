export interface ClientDetails {
    bd: string,
    cdate: Date,
    clientName : string,
    clientShortName : string,
    department: string,
    gstin: string,
    email: string,
    phone: number,
    companyWebsite: string,
    city: string,
    streetAddress: string,
    state: string,
    zipCode: number,
    country: string,
    
}
