import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  
backendUrl = 'http://10.10.12.243:8082/project/get/';
  constructor(private http:HttpClient) { }
  getProject(): Observable<any> {
    return this.http.get<any>(`${this.backendUrl}/project/get/`);
  }
}
