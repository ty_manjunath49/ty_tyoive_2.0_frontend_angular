import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-employee-to-project',
  templateUrl: './employee-to-project.component.html',
  styleUrls: ['./employee-to-project.component.css']
})
export class EmployeeToProjectComponent implements OnInit {
  group2Form: FormGroup;
  mydata: any;

  creatForm: String = 'http://10.10.12.244:8080/mapproject/mapproject';
  myDer: any;
  empolyeeToProjectForm: FormGroup;
  urlTech: String = 'http://10.10.12.244:8080/employee/employee';
  name: { firstname: string; lastname: string; };



  constructor(private fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {

    this.group2Form = this.fb.group({

      projId: this.fb.control('', Validators.required),
      clientEmail: this.fb.control('', Validators.required),
      billableType: this.fb.control('', Validators.required),
      startDate: this.fb.control('', Validators.required),
      poDoc: this.fb.control('', Validators.required),
      sowDoc: this.fb.control('', Validators.required),
      woDoc: this.fb.control('', Validators.required),
      empId: this.fb.control(''),
    });

  }


  idPass(id) {
    console.log(id.value)
    return this.http.get(`${this.urlTech}/${id.value}`).subscribe(res => {
      this.mydata = res;
      console.log(res);
      this.group2Form = this.fb.group({

        projId: this.fb.control('', Validators.required),
        clientEmail: this.fb.control('', Validators.required),
        billableType: this.fb.control('', Validators.required),
        startDate: this.fb.control('', Validators.required),
        poDoc: this.fb.control('', Validators.required),
        sowDoc: this.fb.control('', Validators.required),
        woDoc: this.fb.control('', Validators.required),
        empId: this.fb.control(this.mydata.id.value)
      });

    }, err => {
      console.log(err)
    }, () => {
      console.log('id sent scccessfully')
    });
  }

  get empId(): FormControl {
    return this.group2Form.get('empId') as FormControl;
  }


  get projId(): FormControl {
    return this.group2Form.get('projId') as FormControl;
  }

  get clientEmail(): FormControl {
    return this.group2Form.get('clientEmail') as FormControl;
  }

  get poDoc(): FormControl {
    return this.group2Form.get('poDoc') as FormControl;
  }
  get sowDoc(): FormControl {
    return this.group2Form.get('sowDoc') as FormControl;

  }
  get woDoc(): FormControl {
    return this.group2Form.get('woDoc') as FormControl;
  }
  get billableType(): FormControl {
    return this.group2Form.get('billableType ') as FormControl;
  }
  get startDate(): FormControl {
    return this.group2Form.get('startDate') as FormControl;
  }


  submitForm() {

    console.log(this.group2Form.value);
    this.http.post<any>(`${this.creatForm}`, this.group2Form.value).subscribe(data => {
      console.log(data);

    }, err => {
      console.log(err)
    }, () => {
      console.log('Data sent')
    });

  }


}

