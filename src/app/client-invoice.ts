export interface ClientInvoice {
    _id?:string,
    gstin: string,
    cfullname: string,
    cshortname: string,
    panNo: string,
    cin: string,
    notes: string,
    clientDetails: clientDetails
}

export interface clientDetails{
    _id?:string;
    clientName: string;
    phoneNumber: number;
    email: string;
}