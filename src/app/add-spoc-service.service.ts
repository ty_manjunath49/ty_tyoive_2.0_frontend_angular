import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { backendUrl } from '../app/config';
import { AddSpoc } from './add-spoc';

@Injectable({
  providedIn: 'root'
})
export class AddSpocServiceService {

  constructor(private http: HttpClient) {

   }
   getSPOC(): Observable<any>{
    return this.http.get<any>(`${backendUrl}/getAllClients`);
   }

   insertSPOC(data): Observable<any>{
    return this.http.post<any>(`${backendUrl}/addClientinfo`,data);
   }

   updateSPOC(data): Observable<any>{
    return this.http.put<any>(`${backendUrl}/invoice`,data);
   }

   deleteSPOC(data: AddSpoc): Observable<any>{
    return this.http.delete<any>(`${backendUrl}/invoice`);
   }
}
