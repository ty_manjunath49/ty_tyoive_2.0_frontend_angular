import { TestBed } from '@angular/core/testing';

import { AdclientService } from './adclient.service';

describe('AdclientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdclientService = TestBed.get(AdclientService);
    expect(service).toBeTruthy();
  });
});
