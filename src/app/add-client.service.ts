import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { backendUrl } from '../app/config';
import { ClientInvoice } from './client-invoice';

@Injectable({
  providedIn: 'root'
})
export class AddClientService {

  constructor(private http: HttpClient) { }

  getCompany(): Observable<any>{
    return this.http.get<any>(`${backendUrl}/getAllClients`);
   }

   insertCompany(data): Observable<any>{
    return this.http.post<any>(`${backendUrl}/addClientinfo`,data);
   }

   updateCompany(data): Observable<any>{
    return this.http.put<any>(`${backendUrl}/invoice`,data);
   }

   deleteCompany(data: ClientInvoice): Observable<any>{
    return this.http.delete<any>(`${backendUrl}/invoice`);
   }
}
