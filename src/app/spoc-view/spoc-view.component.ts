import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AddSpoc } from '../add-spoc';
import { AddSpocServiceService } from '../add-spoc-service.service';

@Component({
  selector: 'app-spoc-view',
  templateUrl: './spoc-view.component.html',
  styleUrls: ['./spoc-view.component.css']
})
export class SpocViewComponent implements OnInit {

  spocDetails: AddSpoc = {
    _id: null,
    clientName: null,
    phoneNumber: null,
    email: null,
  };

  SpocDetails: any =[];

  constructor( private addSpoc: AddSpocServiceService,
    private http: HttpClient,
    private router: Router) {
      this.getSpocDetails();
     }

    particularRow(data) {
      console.log(data);
     this.spocDetails = data;
    }

  ngOnInit() {
    this.SpocDetails =Array(168).fill(0).map((x, i) => ({ id: (i + 1), name: `${i + 1}` }));
    $(document).ready(() => {
      $('.clickable-row').click(() => {
        window.location = $(this).data('href');
      });
    });
  }

  onChangePage(spocDetails: Array<any>) {
    this.SpocDetails = spocDetails;
  }

  getSpocDetails(){
    this.addSpoc.getSPOC().subscribe(info => {
      console.log(info);
      this.SpocDetails = info.data;
    }, err => {
      console.log(err);
    }, () => {
      console.log('Spoc Details Got loaded Successfully')
    })
  }






  


}
