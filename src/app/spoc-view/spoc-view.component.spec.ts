import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpocViewComponent } from './spoc-view.component';

describe('SpocViewComponent', () => {
  let component: SpocViewComponent;
  let fixture: ComponentFixture<SpocViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpocViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpocViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
