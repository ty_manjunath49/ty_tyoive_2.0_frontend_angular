import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedEmailComponent } from './shared-email.component';

describe('SharedEmailComponent', () => {
  let component: SharedEmailComponent;
  let fixture: ComponentFixture<SharedEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
