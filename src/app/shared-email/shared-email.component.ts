import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SharedEmailService } from '../shared-email.service';

@Component({
  selector: 'app-shared-email',
  templateUrl: './shared-email.component.html',
  styleUrls: ['./shared-email.component.css']
})
export class SharedEmailComponent implements OnInit {

  message: string;
  error: string;
  errorMessage: string;
  successMessage: string;

  constructor(private sharedEmailService: SharedEmailService) { }

  registerClient(form: NgForm) {
    console.log(form);
    form.resetForm;
    this.sharedEmailService.insertDetails(form.value).subscribe(data => {
      console.log(data);
      this.message = data.message;
      setTimeout(() => {
        this.message = null;
      }, 2000);
      form.reset();
    }, err => {
      console.log(err);
    });
    console.log(' ' + JSON.stringify(form))

  };

  
  ngOnInit() {
  }

}
