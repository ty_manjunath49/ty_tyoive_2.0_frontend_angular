import { TestBed } from '@angular/core/testing';

import { SharedEmailService } from './shared-email.service';

describe('SharedEmailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedEmailService = TestBed.get(SharedEmailService);
    expect(service).toBeTruthy();
  });
});
