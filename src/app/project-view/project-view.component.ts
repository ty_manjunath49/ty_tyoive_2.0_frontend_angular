import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ProjectService } from '../project.service';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.css']
})
export class ProjectViewComponent implements OnInit {

  projectDetails = {
    _id: null,
    projectName: null,
    // invoiceDate: null,
    clientId: null,
    taxLocation: null,
    businessSpoces: null,
    financeSpoces: null,
    invoice: null,
    bankDetails: null
  }

  ProjectDetails: any = [];

  constructor(private http: HttpClient,
    private router: Router,
    private project: ProjectService) {
      this.getProjectDetails();
     }

  ngOnInit() {
    this.ProjectDetails = Array(48).fill(0).map((x, i) => ({ id: (i + 1), name: `${i + 1}` }));
  }

  addEmployee() {
    return this.router.navigateByUrl('add-employee');
  }

  getProjectDetails() {
    this.project.getProject().subscribe(info => {
      console.log(info);
      this.ProjectDetails = info.projects;
      // console.log(info.projects)
   
    }, err => {
      console.log(err);
    }, () => {
      console.log('Project Details got Loaded Successfully')
    })
  }

}
