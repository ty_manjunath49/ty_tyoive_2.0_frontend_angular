import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgForm, FormGroup } from '@angular/forms';
import { AddClientService } from '../add-client.service';
import { ClientInvoice } from '../client-invoice';

@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.css']
})
export class CompanyViewComponent implements OnInit {

  form: FormGroup;

  companyDetails: ClientInvoice = {
    _id: null,
    gstin: null,
    cfullname: null,
    cshortname : null,
    panNo: null,
    cin: null,
    notes: null,
    clientDetails: null
  };

  CompanyDetails: any = [];

  constructor(private addClient: AddClientService,
    private http: HttpClient,
    private router: Router) {
    this.getCompanyDetails();
  }

  particularRow(data) {
    console.log(data);
    this.companyDetails = data;
    localStorage.setItem('clientID',JSON.stringify(data));
    this.router.navigateByUrl('/create_project');
  }

  ngOnInit() {
    this.CompanyDetails = Array(168).fill(0).map((x, i) => ({ id: (i + 1), name: `${i + 1}` }));
    $(document).ready(() => {
      $('.clickable-row').click(() => {
        window.location = $(this).data('href');
      });
    });
  }
  // addSpoc() {
  //   this.router.navigateByUrl('/add-spoc')
  // }

  onChangePage(companyDetails: Array<any>) {
    this.CompanyDetails = companyDetails;
  }

  getCompanyDetails() {
    this.addClient.getCompany().subscribe(info => {
      console.log(info);
      this.CompanyDetails = info.data;
    }, err => {
      console.log(err);
    }, () => {
      console.log('Company Details Got loaded Successfully')
    })
  }

  editForm(x){
      this.form.setValue({
        cfullname:x.cfullname,
        cshortname:x.cshortname,
        gstin:x.gstin,
        panNo:x.panNo,
        cin:x.cin,
        notes:x.notes
      })
    }

  AddProject(data){
    this.companyDetails = data;
    localStorage.setItem('clientID',JSON.stringify(data));
    this.router.navigateByUrl('/create_project');
   }

  // editCompany(){
  //   return this.router.navigateByUrl('edit-company');
  // }

  // deleteCompany(form: NgForm) {
  //   this.addClient.deleteCompany(form.value._id).subscribe(data => {
  //     console.log(data, "The data");
  //     this.getCompanyDetails();
  //     form.reset();
  //     console.log(data);
  //     setTimeout(fr => {
  //       $('#notification').fadeIn('slow').append('Company details deleted successfully');
  //       $('.dismiss').click(() => {
  //         $('#notification').fadeOut('slow');
  //       });
  //     });
  //   }, err => {
  //     console.log(err);
  //   }, () => {
  //     console.log('Company Details deleted successfully');
  //   });
  // }

  // updateCompany(form: NgForm) {
  //   console.log(form.value);
  //   this.addClient.updateCompany(form.value).subscribe(data => {
  //     this.getCompanyDetails();
  //     this.router.navigateByUrl('');
  //     form.reset();
  //   }, err => {
  //     console.log('Company Details Updated Successfully');
  //   });
  // }
}
