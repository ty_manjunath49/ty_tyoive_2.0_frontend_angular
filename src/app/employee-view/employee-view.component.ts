import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {

  employeeDetails = {
    _id: null,
    empId: null,
    empName: null,
    empEmail: null,
    empPhNumber: null,
    technology: null,
    dept: null,
    yoe: null,
    location: null,
    paymentType: null,
    projId: null,
  }

  EmployeeDetails: any = [];

  constructor(private router: Router,
    private employe: EmployeeService) {
      this.getEmployeeDetails();
      
     }

  ngOnInit() {
    this.EmployeeDetails = Array(88).fill(0).map((x, i) => ({ id: (i + 1), name: `${i + 1}` }));
  }
  
  getEmployeeDetails() {
    this.employe.getEmployee().subscribe(info => {
      console.log(info.data);
      this.EmployeeDetails = info.data
    }, err => {
      console.log(err);
    }, () => {
      console.log('Employee Details Got loaded Successfully')
    })
  }


}
