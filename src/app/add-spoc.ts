export interface AddSpoc {
    _id?:string;
    clientName: string;
    phoneNumber: number;
    email: string;
    // invoiceDoorNo: string;
    // invoiceArea: string;
    // invoiceLandmark: string;
    // invoiceTown: string;
    // invoiceState: string;
    // invoiceZip: number;
}
