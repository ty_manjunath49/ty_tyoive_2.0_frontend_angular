import { TestBed } from '@angular/core/testing';

import { AddSpocServiceService } from './add-spoc-service.service';

describe('AddSpocServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddSpocServiceService = TestBed.get(AddSpocServiceService);
    expect(service).toBeTruthy();
  });
});
