import { Component, OnInit } from '@angular/core';
import { AddSpocServiceService } from '../add-spoc-service.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-spoc-client',
  templateUrl: './add-spoc-client.component.html',
  styleUrls: ['./add-spoc-client.component.css']
})
export class AddSpocClientComponent implements OnInit {
  message: string;

  constructor(private spocService: AddSpocServiceService) { }

  ngOnInit() {
  }

  registerUser(form: NgForm) {
    console.log(form);
    this.spocService.insertSPOC(form.value).subscribe(data => {
      console.log(data);
      this.message = data.message;
      setTimeout(() => {
        this.message = null;
      }, 2000);
      form.reset();
    }, err => {
      console.log(err);
    });
}}
