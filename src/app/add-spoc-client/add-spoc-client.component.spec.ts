import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSpocClientComponent } from './add-spoc-client.component';

describe('AddSpocClientComponent', () => {
  let component: AddSpocClientComponent;
  let fixture: ComponentFixture<AddSpocClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddSpocClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSpocClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
