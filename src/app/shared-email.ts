export interface SharedEmail {
    bd: string,
    customer: string,
    skillset: string,
    sdate: Date,
    emailid: string
}
