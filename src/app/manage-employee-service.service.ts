import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ManageEmployeeServiceService {
  backEndUrl = 'http://10.10.12.218:8081';
  constructor(private http: HttpClient, private router: Router) { }

  getUserDetails(username) {
    console.log(username);
    return this.http.get(`${this.backEndUrl}/getEmployeeDetailByName/${username}`);
  }
}
